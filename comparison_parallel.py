
import yaml
import comp_functions
import pandas as pd
import multiprocessing as mp

pd.options.display.width = 1000
pd.set_option("display.max_columns", 101)
pd.set_option("display.max_rows", 101)


#
# Setup
#
FILE = open("configuration.yml", 'r')
CONFIG = yaml.safe_load(FILE)
FUNCTIONS = {col: getattr(comp_functions, CONFIG["comparison functions"][col]) for col in CONFIG['comparison columns']}

base = pd.read_csv(CONFIG['base file'], encoding=CONFIG['base file encoding'], dtype=str)
lookup = pd.read_csv(CONFIG['lookup file'], encoding=CONFIG['lookup file encoding'], dtype=str)
LOOKUP_ORIGINAL = lookup.copy()


#
# Setting up the results df
#
results_headers = ["base file id", "lookup file id", 'total score']

for col in CONFIG['comparison columns']:
    results_headers += [col]

results = pd.DataFrame(index=range(len(base)), columns=results_headers)
results.fillna(0, inplace=True)

# the carry over columns
for col in CONFIG["columns from base in results"]:
    results["base: " + col] = base[col]

for col in CONFIG["columns from lookup in results"]:
    results["lookup: " + col] = 0

temp_scores = pd.DataFrame(index=range(len(lookup)), columns=results_headers)
temp_scores.fillna(0, inplace=True)


#
# Cleaning the base and lookup data frames
#
comp_cols = CONFIG['comparison columns']
base_clean_functions = CONFIG['base file cleaning']
lookup_clean_functions = CONFIG['lookup file cleaning']

for col in base_clean_functions:
    function = getattr(comp_functions, base_clean_functions[col])
    base[col] = base[col].map(function)

for col in lookup_clean_functions:
    function = getattr(comp_functions, lookup_clean_functions[col])
    lookup[col] = lookup[col].map(function)


#
# Helper wrap function to apply pseudo map-reduce row by row on the base
#
def map_wrap(i, q):

    try:
        temp_scores["total score"] = 0

        for col in comp_cols:
            function = FUNCTIONS[col]

            # updating cols with new values
            temp_scores[col] = lookup[col].map(lambda x: function(base.loc[i, col], x))

        for col in CONFIG["score weights"]:
            temp_scores['total score'] += CONFIG["score weights"][col] * temp_scores[col]

        # filling the final results with appropriate values
        max_lookup_index = temp_scores["total score"].idxmax(axis=1)
        results.iloc[i, 0:len(comp_cols) + 3] = temp_scores.iloc[max_lookup_index]
        results.loc[i, "base file id"] = base.loc[i, CONFIG["base file id col"]]
        results.loc[i, "lookup file id"] = lookup.loc[max_lookup_index, CONFIG["lookup file id col"]]

        # adding the carry over columns from base and lookup that are in results
        for col in CONFIG["columns from lookup in results"]:
            results.loc[i, "lookup: " + col] = LOOKUP_ORIGINAL.loc[max_lookup_index, col]

        q.put(list(results.loc[i]))

        print("row:", i + 2, "complete")

    except Exception as e:
        print("row:", i + 2, "error ", e)


if __name__ == "__main__":
    pp = mp.Pool(processes=CONFIG["num processes"])
    m = mp.Manager()
    q = m.Queue()

    pp.starmap(map_wrap, [(i, q) for i in range(len(results))], chunksize=CONFIG["starmap chunksize"])

    queue_results = []
    while q.empty() is False:
        queue_results += [q.get()]

    csv = pd.DataFrame(queue_results, columns=list(results))
    csv.to_csv(CONFIG["results file"])

    pp.close()
    exit()

