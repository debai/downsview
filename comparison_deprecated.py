
import yaml
import comp_functions
import pandas as pd

import time
start = time.time()
pd.options.display.width = 1000
pd.set_option("display.max_columns", 101)
pd.set_option("display.max_rows", 101)

file = open("data/configuration.yml", 'r')
config = yaml.safe_load(file)

base = pd.read_csv(config['base file'], encoding=config['base file encoding'], dtype=str)
lookup = pd.read_csv(config['lookup file'], encoding=config['lookup file encoding'], dtype=str)
lookup_orignal = lookup.copy()

results_headers = ["base file id", "lookup file id", 'total score']
for col in config['comparison columns']:
    results_headers += [col]

results = pd.DataFrame(index=range(len(base)), columns=results_headers)
results.fillna(0, inplace=True)

# the carry over columns
for col in config["columns from base in results"]:
    results["base: " + col] = base[col]

for col in config["columns from lookup in results"]:
    results["lookup: " + col] = 0

temp_scores = pd.DataFrame(index=range(len(lookup)), columns=results_headers)
temp_scores.fillna(0, inplace=True)

comp_cols = config['comparison columns']
base_clean_functions = config['base file cleaning']
lookup_clean_functions = config['lookup file cleaning']


# Cleaning the base and lookup data frames
for col in base_clean_functions:
    function = getattr(comp_functions, base_clean_functions[col])
    base[col] = base[col].map(function)

for col in lookup_clean_functions:
    function = getattr(comp_functions, lookup_clean_functions[col])
    lookup[col] = lookup[col].map(function)


# applying pseudo map-reduce row by row on the base
for i in range(len(results)):

    print("row number in base_csv: ",  i + 2)

    temp_scores["total score"] = 0

    for col in comp_cols:
        function = getattr(comp_functions, config["comparison functions"][col])

        # updating cols with new values
        temp_scores[col] = lookup[col].map(lambda x: function(base.loc[i, col], x))

    for col in config["score weights"]:
        temp_scores['total score'] += config["score weights"][col] * temp_scores[col]

    # filling the final results with appropriate values
    max_lookup_index = temp_scores["total score"].idxmax(axis=1)
    results.iloc[i, 0:len(comp_cols) + 3] = temp_scores.iloc[max_lookup_index]
    results.loc[i, "base file id"] = base.loc[i, config["base file id col"]]
    results.loc[i, "lookup file id"] = lookup.loc[max_lookup_index, config["lookup file id col"]]

    # adding the carry over columns from base and lookup that are in results
    for col in config["columns from lookup in results"]:
        results.loc[i, "lookup: " + col] = lookup_orignal.loc[max_lookup_index, col]


results.to_csv("results.csv", index=False)


print(time.time() - start)
