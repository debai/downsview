
import string
import numpy as np


def oclc_clean(cell):

    if cell is np.nan:
        return np.nan

    cell = cell.lower()
    cell = cell.split(";")
    cell = list(map(lambda x: x.translate(str.maketrans('', '', string.punctuation)), cell))
    cell = list(map(lambda x: x.replace(" ", ""), cell))

    return cell


def general_clean(cell):

    if cell is np.nan:
        return np.nan

    cell = cell.lower()
    cell = cell.split(" ")
    cell = list(filter(lambda x: len(x) > 0, cell))
    cell = list(map(lambda x: x.translate(str.maketrans('', '', string.punctuation)), cell))
    cell = list(map(lambda x: x.replace(" ", ""), cell))

    return cell

###########################
# Comparison Functions


def oclc_compare(base_oclcs, lookup_oclcs):

    if isinstance(base_oclcs, list) is False or isinstance(lookup_oclcs, list) is False:
        return 0


    for base_oclc in base_oclcs:
        if base_oclc in lookup_oclcs:
            return 1
        else:
            return 0


def author(base, lookup):

    character_count = 0
    correct_character_count =0
    answer = 0

    if isinstance(base, list) is False or isinstance(lookup, list) is False:
        return 0

    for word in base:
        character_count += len(word)
        if word in lookup:
            correct_character_count += len(word)

    if character_count > 0:
        answer = correct_character_count / character_count

    return answer


def title(base, lookup):

    character_count = 0
    correct_character_count =0
    answer = 0

    if isinstance(base, list) is False or isinstance(lookup, list) is False:
        return 0

    for word in base:
        character_count += len(word)
        if word in lookup:
            correct_character_count += len(word)

    if character_count > 0:
        answer = correct_character_count / character_count

    return answer


def date_compare(base, lookup):

    answer = 0
    character_count = 0
    correct_character_count = 0


    if isinstance(base, list) is False or isinstance(lookup, list) is False:
        return 0


    for word in base:
        character_count += len(word)
        if word in lookup:
            correct_character_count += len(word)

    if character_count > 0:
        answer = correct_character_count / character_count

    return answer


def pages(base, lookup):

    character_count = 0
    correct_character_count = 0
    answer = 0

    if isinstance(base, list) is False or isinstance(lookup, list) is False:
        return 0

    for word in base:
        character_count += len(word)
        if word in lookup:
            correct_character_count += len(word)

    if character_count > 0:
        answer = correct_character_count / character_count

    return answer



